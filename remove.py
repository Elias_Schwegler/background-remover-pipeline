import os
import glob
import time
from PIL import Image
from rembg import remove

# Define your paths
input_folder = r'C:\ai\pinokio\api\sd-webui.pinokio.git\automatic1111\outputs\txt2img-images'
output_folder = r'A:\A OUT REMOVED BACKGROUNDS'

# Ensure output directory exists
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

# Processed files tracker
processed_files = set()

# Function to process images
def process_images():
    global processed_files
    image_files = glob.glob(input_folder + '/**/*.png', recursive=True)

    for image_file in image_files:
        if image_file in processed_files:
            continue  # Skip already processed files

        try:
            img = Image.open(image_file)
            img_no_bg = remove(img)
            output_file_path = os.path.join(output_folder, os.path.basename(image_file))
            img_no_bg.save(output_file_path)
            print(f'Background successfully removed {os.path.basename(image_file)}')
            processed_files.add(image_file)
        except Exception as e:
            print(f'Background failed to remove {os.path.basename(image_file)}: {str(e)}')

# Run continuously
while True:
    process_images()
    time.sleep(60)  # Wait for 60 seconds before checking again
